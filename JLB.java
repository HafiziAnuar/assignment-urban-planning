/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jlb;

import java.awt.BorderLayout;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author user
 */
public class JLB {

// JOHNNYLAND BLUEPRINT
    public static void main(String[] args) {
        System.out.println("WELCOME TO JOHNNYLAND CITY!");
        System.out.println("GENERATING JOHNNYLAND BLUEPRINT\n");
        Scanner s = new Scanner(System.in);
        String[][] Building = new String[30][30];
        int[] size = new int[26];
        int counter = 0;

        //name and range is what will appear in the question to the user
        String[] name = {"museum", "bank", "hotel", "mall (1)", "housing area (1)", "school", "housing area (2)", "theme park", "Johnny Convention Centre",
            "park", "office", "court house", "police station", "fire station", "mall (2)", "church", "hospital", "mosque", "temple", "waste water treatment",
            "condominium (1)", "university", "condominium (2)", "stadium", "airport", "Johnny Subway Station",};
        String[] range = {"9-12", "6-8", "6-9", "8-12", "8-10", "6-9", "8-10", "10-15", "6-8", "15-20", "10-12", "6-8", "6-9", "6-9", "8-12", "6-9",
            "10-12", "6-9", "6-8", "6-8", "6-8", "6-9", "6-8", "6-9", "15-20", "6-9"};

        //lowerRange and upperRange are for the while statement determining the true/false input by the user
        int[] lowerRange = {9, 6, 6, 8, 8, 6, 8, 10, 6, 15, 10, 6, 6, 6, 8, 6, 10, 6, 6, 6, 6, 6, 6, 6, 15, 6};
        int[] upperRange = {12, 8, 9, 12, 10, 9, 10, 15, 8, 20, 12, 8, 9, 9, 12, 9, 12, 9, 8, 8, 8, 9, 8, 9, 20, 9};

        //matrix dimensions are iRow and jCol, while the dimension limit are rowLimit and colLimit
        int[] iRow = {0, 5, 7, 12, 17, 20, 27, 0, 6, 11, 20, 20, 26, 26, 2, 7, 14, 20, 26, 0, 5, 12, 17, 21, 26, 12};
        int[] jCol = {0, 0, 2, 1, 0, 2, 0, 7, 7, 9, 7, 12, 7, 11, 16, 20, 16, 16, 17, 26, 26, 24, 25, 24, 24, 20};
        int[] rowLimit = {3, 9, 10, 15, 19, 23, 29, 3, 8, 15, 24, 24, 29, 29, 8, 10, 18, 23, 30, 2, 9, 15, 19, 24, 30, 15};
        int[] colLimit = {4, 2, 5, 5, 5, 5, 5, 12, 11, 14, 10, 14, 10, 14, 18, 23, 19, 19, 19, 30, 28, 27, 29, 27, 29, 23};

        //the initials representing the buildings (ArrayList was used for programmers' easy reference)
        ArrayList<String> al = new ArrayList<String>();
        al.add("MU");    //museum
        al.add("B");     //bank
        al.add("H");     //hotel
        al.add("M");     //mall
        al.add("HA");    //housing area
        al.add("S");     //school
        al.add("HA");    //housing area
        al.add("TH");    //theme park
        al.add("JCC");   //Johnny Convention Centre
        al.add("P");     //park
        al.add("OFF");   //office
        al.add("CRT");   //courtHouse
        al.add("PS");    //police station
        al.add("FS");    //fire station
        al.add("M");     //mall
        al.add("CH");    //church
        al.add("HS");    //hospital
        al.add("MS");    //mosque
        al.add("T");     //temple
        al.add("WWT");   //waste water treatment
        al.add("C");     //condo
        al.add("U");     //university
        al.add("C");     //condo
        al.add("ST");    //stadium
        al.add("A");     //airport
        al.add("JSS");   //Johnny Subway Station

        //to reset the data in the text file so that no old appended data being re-used in the program 
        try {
            PrintWriter reset = new PrintWriter(new FileOutputStream("data.txt"));
            reset.close();
        } catch (IOException e) {
            System.out.println("Problem with file output");
        }

        //question to the user asking for input
        //name[i] will scan the name array in order. the same with range[i]
        //lowerRange[i] and upperRange[i] will also scan their respective arrays 
        //size[i] will be from user input
        for (int i = 0; i < 26; i++) {
            System.out.printf("What is the size for " + name[i] + "? (Range = " + range[i] + ")\n");
            size[i] = s.nextInt();
            while (size[i] < lowerRange[i] || size[i] > upperRange[i]) {
                System.out.printf("ERROR! Please try again. Enter the size for " + name[i] + ". (Range = " + range[i] + ")\n");
                size[i] = s.nextInt();
            }

            //user input data are transferred to text file "data.txt" using the method userInput()
            int dataInput = userInput(size[i]);
        }

        //reading from "data.txt" text file using the method readFile()
        int[] useOutput = readFile("data.txt");

        //buildings method
        String[][] simpleBuild = buildingBlocks(useOutput, rowLimit, colLimit, iRow, jCol, al);

        //using GUI to map the blueprint
        Object columnNames[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21",
            "22", "23", "24", "25", "26", "27", "28", "29"};
        JTable blueprint = new JTable(simpleBuild, columnNames);
        JScrollPane scrollPane = new JScrollPane(blueprint);

        blueprint.setTableHeader(null);
        JFrame frame = new JFrame();
        frame.setTitle("JohnnyLand Blueprint");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.setSize(1500, 1500);
        frame.setVisible(true);

        //JOHNNYLAND UNDERGROUND SUBWAY MAP
        System.out.println("\nGENERATING JOHNNYLAND SUBWAY MAP\n");

        //matrix dimensions are iRow and jCol, while the dimension limit are rowLimit and colLimit
        int[] iiRow = {26, 0, 12, 2, 6, 12, 17, 27};
        int[] jjCol = {24, 7, 1, 16, 7, 20, 0, 0};
        int[] rrowLimit = {30, 3, 15, 8, 8, 15, 19, 29};
        int[] ccolLimit = {29, 12, 5, 18, 11, 23, 5, 5};
        int[] seq = {20, 15, 12, 12, 8, 9, 10, 10};

        ArrayList<String> arl = new ArrayList<String>();
        arl.add(al.get(24));    //airport
        arl.add(al.get(7));     //theme park
        arl.add(al.get(3));     //mall
        arl.add(al.get(14));    //mall
        arl.add(al.get(8));     //Johnny Convention Centre
        arl.add(al.get(25));    //Johnny Subway Station
        arl.add(al.get(6));     //housing area
        arl.add(al.get(4));     //housing area

        System.out.println(" The connecting station to Johnny subway lines are\n"
                + " airport,\n theme park,\n housing area,\n mall,\n Johnny Convention Centre\n"
                + " and Johnny Subway Station.");

        String[][] Buildings = new String[30][30];

        iRow = iiRow;
        jCol = jjCol;
        rowLimit = rrowLimit;
        colLimit = ccolLimit;
        al = arl;
        useOutput = seq;

        String[][] simplesub = buildingBlocks(useOutput, rowLimit, colLimit, iRow, jCol, al);

        JTable subway = new JTable(simplesub, columnNames);
        JScrollPane scrolPane = new JScrollPane(subway);

        subway.setTableHeader(null);
        JFrame sframe = new JFrame();
        sframe.setTitle("Johnnyland Subway Map");
        sframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        sframe.add(scrolPane, BorderLayout.CENTER);
        sframe.setSize(1500, 1500);
        sframe.setVisible(true);

    }

    //method to get user input and put in text file
    public static int userInput(int size) {
        try {
            PrintWriter data = new PrintWriter(new FileOutputStream("data.txt", true));
            data.print(size + "\t");
            data.close();
        } catch (IOException e) {
            System.out.println("Problem with file output");
        }
        return size;
    }

    //method to read text file
    public static int[] readFile(String data) {
        try {
            Scanner s = new Scanner(new FileInputStream(data));
            int ctr = 0;
            while (s.hasNext()) {
                ctr++;
                s.nextInt();
            }
            Scanner s1 = new Scanner(new FileInputStream(data));
            int arr[] = new int[ctr];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = s1.nextInt();
            }
            s.close();
            s1.close();
            return arr;
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    //method to generate the buildings
    // codes for the buildings
    // val will scan user's input from the text file
    // rL, cL, row and col will scan the parameters of the buildings from the respective arrays
    public static String[][] buildingBlocks(int[] useOutput, int[] rowLimit,
            int[] colLimit, int[] iRow, int[] jCol, ArrayList<String> al) {
        String[][] Building = new String[30][30];
        int counter = 0;
        //main roads
        for (int i = 0; i < 30; i++)//row
        {
            for (int j = 0; j < 30; j++)//column
            {
                Building[i][j] = " ";
                if (j == 5 || j == 14) {
                    Building[i][j] += "^";
                } else if (j == 6 || j == 15) {
                    Building[i][j] += "v";
                } else if (i == 25) {
                    Building[i][j] += ">";
                } else if (i == 24) {
                    Building[i][j] += "<";
                } else if (j == 23) {
                    Building[i][j] += "^v";
                } else if (i == 10 && j > 6) {
                    Building[i][j] += ">";
                } else if (i == 11 && j > 14) {
                    Building[i][j] += "<";
                } else if (i == 15 && j > 6 && j < 14) {
                    Building[i][j] += "<>";
                }
            }
        }

        for (int count = 0; count < useOutput.length; count++) {
            int val = useOutput[count];
            int rL = rowLimit[count];
            int cL = colLimit[count];
            int row = iRow[count];
            int col = jCol[count];

            for (int i = row; i < rL; i++) {

                for (int j = col; j < cL; j++) {
                    Building[i][j] = al.get(count);
                    counter++;
                    if (counter == val) {
                        break;
                    }
                }
                if (counter == val) {
                    break;
                }
            }
            counter = 0;
        }
        if (useOutput.length > 20) {
            //small roads 
            Building[1][4] = "<>";
            Building[5][2] = "<>";
            Building[5][3] = "<>";
            Building[5][4] = "<>";
            Building[6][24] = "<>";
            Building[6][25] = "<>";
            Building[1][25] = "<>";
            Building[1][24] = "<>";
            Building[18][24] = "<>";

            Building[10][14] = "^>";
            Building[10][15] = "v>";

            Building[25][5] += ">";
            Building[25][6] += ">";
            Building[25][14] += ">";
            Building[25][15] += ">";
            Building[10][23] += ">";

            Building[24][23] += "^v";
            Building[25][23] += "^v";

            Building[11][23] += "<";
            Building[24][5] += "<";
            Building[24][6] += "<";
            Building[24][14] += "<";
            Building[24][15] += "<";
        }

        return Building;
    }
}
