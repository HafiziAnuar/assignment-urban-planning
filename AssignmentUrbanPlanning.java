/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.BorderLayout;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author wig190017
 */
public class AssignmentUrbanPlanning {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String A ="A";
        String H ="H";
        String PS ="PS";
        String FS ="FS";
        String M ="M";
        String S ="S";
        String OFF="OFF";
        String TH = "TH";
        String B = "B";
        String MU = "MU";
        String P = "P";
        String CRT = "CRT";
        String HA = "HA";
        String T = "T";
        String ST = "ST";
        String C = "C";
        String MS = "MS";
        String U = "U";
        String CH = "CH";
        String WWT = "WWT";
        String JCC = "JCC"; 
        String HS = "HS";
        Scanner sc = new Scanner (System.in);
        
        System.out.println("What is the size for Airport? range = 15-20" );
        int sA = sc.nextInt();
        System.out.println("What is the size for Hotel? range = 6-9");
        int sH = sc.nextInt();
        System.out.println("What is the size for School? range = 6-9" );
        int sS = sc.nextInt();
        System.out.println("What is the size for Theme Park? range = 10-15");
        int sTH = sc.nextInt();
        System.out.println("What is the size for Bank? range = 6-8" );
        int sB = sc.nextInt();
        System.out.println("What is the size for Museum? range = 9-12");
        int sMU = sc.nextInt();
        System.out.println("What is the size for Park? range = 15-20" );
        int sP = sc.nextInt();
        System.out.println("What is the size for Court? range = 6-8");
        int sCRT = sc.nextInt();
        System.out.println("What is the size for Housing Area? range = 8-10" );
        int sHA = sc.nextInt();
        System.out.println("What is the size for Temple? range = 6-8");
        int sT = sc.nextInt();
        System.out.println("What is the size for Stadium? range = 6-9" );
        int sST = sc.nextInt();
        System.out.println("What is the size for Condominium? range = 6-8");
        int sC = sc.nextInt();
        System.out.println("What is the size for Masjid? range = 6-9" );
        int sMS = sc.nextInt();
        System.out.println("What is the size for Mall? range = 8-12");
        int sM = sc.nextInt();
        System.out.println("What is the size for Uni? range = 6-9" );
        int sU = sc.nextInt();
        System.out.println("What is the size for Church? range = 6-9");
        int sCH = sc.nextInt();
        System.out.println("What is the size for Waste Water Treatment? range = 6-8" );
        int sWWT = sc.nextInt();
        System.out.println("What is the size for Police Station? range = 6-8");
        int sPS = sc.nextInt();
        System.out.println("What is the size for Fire Station? range = 6-8" );
        int sFS = sc.nextInt();
        System.out.println("What is the size for Johnny Convention Centre? range = 6-8");
        int sJCC = sc.nextInt();
        System.out.println("What is the size for Office Building? range = 10-12");
        int sOFF = sc.nextInt();
        System.out.println("What is the size for Hospital? range = 10-12");
        int sHS = sc.nextInt();
        
        String [][]Building=new String[30][30];

        for (int i=0;i<30;i++)//ketepi
        {
            for (int j=0;j<30;j++)//kebawah
            {
                Building[i][j]=" ";
                if (j==5 || j==14)
                    Building[i][j]+="^";
                else if (j==6 || j==15)
                    Building[i][j]+="v";
                else if (i==25)
                    Building[i][j]+=">";
                else if (i==24)
                    Building[i][j]+="<";
                else if (j==23)
                    Building[i][j]+="^v";
                else if (i==10 && j>6)
                    Building[i][j]+=">";
                else if (i==11 && j>14)
                    Building[i][j]+="<";
                else if (i==15 && j>6 && j<14)
                    Building[i][j]+="<>";
                    }
        }
        int counter=0;
            for (int i=26;i<30;i++)
            {                
                for (int j=24;j<29;j++)
                {
                    Building[i][j]=A;
                    counter++;
                if (counter==sA)
                break;                   
                }
                if (counter==sA)
                break;
            }
                   counter=0;
            for (int i=9;i>6;i--)
            {                
                for (int j=4;j>1;j--)
                {
                    Building[i][j]=H;
                    counter++;
                if (counter==sH)
                break;                   
                }
                if (counter==sH)
                break;
            }
                     counter=0;
            for (int i=22;i>18;i--)
            {                
                for (int j=4;j>1;j--)
                {
                    Building[i][j]=S;
                    counter++;
                if (counter==sS)
                break;                   
                }
                if (counter==sS)
                break;
            }
                     counter=0;
            for (int i=0;i<3;i++)
            {                
                for (int j=7;j<12;j++)
                {
                    Building[i][j]=TH;
                    counter++;
                if (counter==sTH)
                break;                   
                }
                if (counter==sTH)
                break;
            }
                     counter=0;
            for (int i=5;i<9;i++)
            {                
                for (int j=0;j<2;j++)
                {
                    Building[i][j]=B;
                    counter++;
                if (counter==sB)
                break;                   
                }
                if (counter==sB)
                break;
            }
            ////////////////////////////////////
               counter=0; 
            for (int i=2;i>=0;i--)
            {                
                for (int j=3;j>=0;j--)
                {
                    Building[i][j]=MU;
                    counter++;
                if (counter==sMU)
                break;                   
                }
                if (counter==sMU)
                break;
            }
               counter=0;
            for (int i=11;i<15;i++)
            {                
                for (int j=9;j<14;j++)
                {
                    Building[i][j]=P;
                    counter++;
                if (counter==sP)
                break;                   
                }
                if (counter==sP)
                break;
            }
               counter=0;
            for (int i=23;i>19;i--)
            {                
                for (int j=11;j<13;j++)
                {
                    Building[i][j]=CRT;
                    counter++;
                if (counter==sCRT)
                break;                   
                }
                if (counter==sCRT)
                break;
            }
               counter=0;
            for (int i=18;i>16;i--)
            {                
                for (int j=4;j>=0;j--)
                {
                    Building[i][j]=HA;
                    counter++;
                if (counter==sHA)
                break;                   
                }
                if (counter==sHA)
                break;
            }
             counter=0;
            for (int i=28;i<30;i++)
            {                
                for (int j=4;j>=0;j--)
                {
                    Building[i][j]=HA;
                    counter++;
                if (counter==sHA)
                break;                   
                }
                if (counter==sHA)
                break;
            }
               counter=0;
            for (int i=26;i<30;i++)
            {                
                for (int j=17;j<19;j++)
                {
                    Building[i][j]=T;
                    counter++;
                if (counter==sT)
                break;                   
                }
                if (counter==sT)
                break;
            }
            //////////////////////////////////////////
               counter=0;
            for (int i=23;i>20;i--)
            {                
                for (int j=24;j<27;j++)
                {
                    Building[i][j]=ST;
                    counter++;
                if (counter==sST)
                break;                   
                }
                if (counter==sST)
                break;
            }
               counter=0;
            for (int i=8;i>4;i--)
            {                
                for (int j=26;j<28;j++)
                {
                    Building[i][j]=C;
                    counter++;
                if (counter==sC)
                break;                   
                }
                if (counter==sC)
                break;
            }
               counter=0;
            for (int i=18;i>16;i--)
            {                
                for (int j=25;j<29;j++)
                {
                    Building[i][j]=C;
                    counter++;
                if (counter==sC)
                break;                   
                }
                if (counter==sC)
                break;
            }            
               counter=0;
            for (int i=22;i>19;i--)
            {                
                for (int j=17;j<20;j++)
                {
                    Building[i][j]=MS;
                    counter++;
                if (counter==sMS)
                break;                   
                }
                if (counter==sMS)
                break;
            }
               counter=0;
            for (int i=12;i<15;i++)
            {                
                for (int j=4;j>0;j--)
                {
                    Building[i][j]=M;
                    counter++;
                if (counter==sM)
                break;                   
                }
                if (counter==sM)
                break;
            }
            counter=0;
            for (int i=2;i<8;i++)
            {                
                for (int j=16;j<18;j++)
                {
                    Building[i][j]=M;
                    counter++;
                if (counter==sM)
                break;                   
                }
                if (counter==sM)
                break;
            }
               counter=0;
            for (int i=12;i<15;i++)
            {                
                for (int j=24;j<27;j++)
                {
                    Building[i][j]=U;
                    counter++;
                if (counter==sU)
                break;                   
                }
                if (counter==sU)
                break;
            }
            ////////////////////////////////////////////
               counter=0;
            for (int i=9;i>6;i--)
            {                
                for (int j=22;j>19;j--)
                {
                    Building[i][j]=CH;
                    counter++;
                if (counter==sCH)
                break;                   
                }
                if (counter==sCH)
                break;
            }
               counter=0;
            for (int i=1;i>=0;i--)
            {                
                for (int j=26;j<30;j++)
                {
                    Building[i][j]=WWT;
                    counter++;
                if (counter==sWWT)
                break;                   
                }
                if (counter==sWWT)
                break;
            }
               counter=0;
            for (int i=26;i<28;i++)
            {                
                for (int j=7;j<11;j++)
                {
                    Building[i][j]=PS;
                    counter++;
                if (counter==sPS)
                break;                   
                }
                if (counter==sPS)
                break;
            }
               counter=0;
            for (int i=28;i<30;i++)
            {                
                for (int j=7;j<11;j++)
                {
                    Building[i][j]=FS;
                    counter++;
                if (counter==sFS)
                break;                   
                }
                if (counter==sFS)
                break;
            }
               counter=0;
            for (int i=6;i<8;i++)
            {                
                for (int j=7;j<11;j++)
                {
                    Building[i][j]=JCC;
                    counter++;
                if (counter==sJCC)
                break;                   
                }
                if (counter==sJCC)
                break;
            }
               counter=0;
            for (int i=20;i<24;i++)
            {                
                for (int j=7;j<10;j++)
                {
                    Building[i][j]=OFF;
                    counter++;
                if (counter==sOFF)
                break;                   
                }
                if (counter==sOFF)
                break;
            }
               counter=0;
            for (int i=14;i<18;i++)
            {                
                for (int j=16;j<19;j++)
                {
                    Building[i][j]=HS;
                    counter++;
                if (counter==sHS)
                break;                   
                }
                if (counter==sHS)
                break;
            }
               
        Building[2][4]="<>";
        Building[5][2]="<>";
        Building[5][3]="<>";
        Building[5][4]="<>";
        
        Building[10][14]="^>";
        Building[10][15]="v>";
        Building[18][24]="<>";
        Building[9][26]="^v";
        
        Building[1][25]="<>";
        Building[1][24]="<>";
        
        Building[24][5]+="<";
        Building[24][6]+="<";
        Building[25][5]+=">";
        Building[25][6]+=">";

        Building[10][23]+=">";
        Building[11][23]+="<";
        
        Building[24][23]+="^v";
        Building[25][23]+="^v";

        Building[24][14]+="<";
        Building[24][15]+="<";
        Building[25][14]+=">";
        Building[25][15]+=">";
        
    Object columnNames[] =  {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24", "25", "26", "27", "28", "29"}; 
    JTable table = new JTable(Building, columnNames);
    JScrollPane scrollPane = new JScrollPane(table);

    table.setTableHeader(null);
        
    frame.add(scrollPane, BorderLayout.CENTER);
    frame.setSize(300, 300);
    frame.setVisible(true);
        // TODO code application logic here


    JFrame frame1 = new JFrame();
    frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Object rowData[][] = {
        {" "," "," ","V<","<","<","<","TH","TH","TH","TH","TH",">",">",">",">",">",">V"," "," "," "," "," "," "," "," "," "," "," "," "},
        {" "," "," ","V","^>",">",">","TH","TH","TH","TH","TH","<","<","<","<","<^","V"," "," "," "," "," "," "," "," "," "," "," "," "},//DONE
        {" "," "," ","V","^"," "," ","TH","TH","TH","TH","TH"," "," "," "," ","M","M"," "," "," "," "," "," "," "," "," "," "," "," "},//DONE
        {" "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "," ","M","M","",""," "," "," "," "," "," "," "," "," "," "},//DONE
        {" "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "," ","M","M",""," "," "," "," "," "," "," "," "," "," "," "},//DONE
        {" "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "," ","M","M "," "," "," "," "," "," "," "," "," "," "," "," "},//DONE
        
        {" "," "," ","V","^<","<","<","JCC","JCC","JCC","JCC","<","<","<","<","<","M","M",""," "," "," "," "," "," "," "," "," "," "," "},//DONE
        {" "," "," ","V>","^>",">",">","JCC","JCC","JCC","JCC",">",">",">",">",">","M","M"," "," "," "," "," "," "," "," "," "," "," "," "},//DONE
        {" "," "," ","V","^"," "," "," ","^","V"," "," ","V","^"," "," ","M","M"," "," "," "," "," "," "," "," "," "," "," "," "},//DONE
        {" "," "," ","V","^"," "," "," ","^","V"," "," ","V","^"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},//DONE
        {" "," "," ","V","^"," "," "," ","^","V"," "," ","V","^"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
        
        {" "," "," ","V","^"," "," "," ","^","V"," "," "," ","^"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
        {" ","M","M","M","M"," "," "," ","^","V"," "," ","V","^"," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "},
        {" ","M","M","M","M",">",">",">",">^",">V",">",">",">v",">^",">",">",">",">",">","^>",">",">",">",">","U","U","U"," "," "," "},
        {" ","M","M","M","M","<","<","<","^<","V<","<","<","<V","<^","<","<","<","<","V<","^<","<","<","<","<","U","U","U"," "," "," "},
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","v","^"," "," "," "," ","U","U","U"," "," "," "},
       
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        {"HA","HA","HA","HA","HA",">",">",">",">^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        {"HA","HA","HA","HA","HA","<","<","<","^<","<V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},
        
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," "," "," "," "," "," "," "},//DONE
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," ","A","A","A","A","A"," "},//DONE
        {" ","v","^"," "," "," "," "," ","^","V"," "," "," "," "," "," "," "," ","V","^"," "," "," "," ","A","A","A","A","A"," "},//DONE
        {"HA","HA","HA","HA","HA",">",">",">",">^",">V",">",">",">",">",">",">",">",">",">v",">^",">",">",">",">","A","A","A","A","A"," "},//DONE
        {"HA","HA","HA","HA","HA","<","<","<","<^<","<V","<","<","<","<","<","<","<","<","<V","<^","<","<","<","<","A","A","A","A","A"," "},//DONE
 
    };
    Object columnName[] =  {"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29"}; 
    JTable table1 = new JTable(rowData, columnNames);
    JScrollPane scrollPane1 = new JScrollPane(table1);

    table1.setTableHeader(null);
        
    frame1.add(scrollPane1, BorderLayout.CENTER);
    frame1.setSize(300, 150);
    frame1.setVisible(true);
        // TODO code application logic here
    }
    
}
